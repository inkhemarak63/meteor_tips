import Tabular from 'meteor/aldeed:tabular';
import { Partners } from '../collections';
// import Util from '/configs/util';
new Tabular.Table({
	name: 'MemberListTable',
	collection: Partners,
	responsive: true,
	autoWidth: false,
	stateSave: false,
	pageLength: 25,
	extraFields: ['userId', 'isSuperAdmin'],
	columns: [
        { data: "name", title: "Name" },
        { data: "email", title: "Email" },
    ]
  });