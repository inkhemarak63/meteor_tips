import Tabular from 'meteor/aldeed:tabular';
import { Province } from '../collections';
// import Util from '/configs/util';
new Tabular.Table({
	name: 'MemberListTable',
	collection: Province,
	responsive: true,
	autoWidth: false,
	stateSave: false,
	pageLength: 25,
    columns: [
        { data: "name", title: "Name" },
        { data: "view_order", title: "View order" },
        {
            data: "_id",
            title: "Edit",
            render: function(e) {
                return Spacebars.SafeString('<a href="#" id="edit_province" data-value=' + e + ' class="btn btn-primary fa fa-pencil btn-sm" style="cursor: pointer;"> Edit</a>');
            }
        },
        {
            data: "_id",
            title: "Delete",
            render: function(e) {
                console.log(e);
                return Spacebars.SafeString('<a href="#" id="delete_province" data-id=' + e + ' class="btn btn-danger fa fa-trash btn-sm" style="cursor: pointer;"> Delete</a>');
            }
        }

    ]
  });