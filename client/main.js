import '/configs/routes';
import '/configs/user';
import '/libs/helpers';
import '/libs/tabulars';

import { Template } from 'meteor/templating';
import { $ } from 'meteor/jquery';
import dataTablesBootstrap from 'datatables.net-bs';
dataTablesBootstrap(window, $);

import './main.html';

//publish all
Meteor.subscribe('PublishUserLogin', Meteor.userId());

Template.registerHelper('userLogin', function(id) {
    if (Meteor.userId() !== null) return true;
    return false;
});

if (Meteor.isClient) {
    Meteor.startup(function() {
        
    });
}