import './loading.html';

Template.loading.onRendered(function() {
  if (!Session.get('loadingSplash')) {
    let spinner = '<div class="sk-spinner sk-spinner-wave">'
      +  '<div class="rect sk-rect1"></div>'
      +  '<div class="rect sk-rect2"></div>'
      +  '<div class="rect sk-rect3"></div>'
      +  '<div class="rect sk-rect4"></div>'
      +  '<div class="rect sk-rect5"></div>'
      + '</div>';

    this.loading = window.pleaseWait({
      logo: '/logo-tips2.png',
      backgroundColor: 'rgb(30 115 189)',
      loadingHtml: spinner
    });
    Session.set('loadingSplash', true);
  }
});

Template.loading.onDestroyed(function() {
  if (this.loading) {
    this.loading.finish();

    // for beside chorme
    setTimeout(() => {
      $(this.loading._loadingElem).remove();
    }, 1800);
  }
});
